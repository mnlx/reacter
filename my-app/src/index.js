import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Apper from './Apper';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
